package ir.nwise.nwisemediaplayer;

public interface NWiseVideoProgressCallback {

    void onVideoProgressUpdate(int position, int duration);
}
