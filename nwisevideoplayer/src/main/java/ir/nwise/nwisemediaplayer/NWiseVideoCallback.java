package ir.nwise.nwisemediaplayer;

import android.net.Uri;


public interface NWiseVideoCallback {

    void onStarted(NWiseVideoPlayer player);

    void onPaused(NWiseVideoPlayer player);

    void onPreparing(NWiseVideoPlayer player);

    void onPrepared(NWiseVideoPlayer player);

    void onBuffering(int percent);

    void onError(NWiseVideoPlayer player, Exception e);

    void onCompletion(NWiseVideoPlayer player);

    void onRetry(NWiseVideoPlayer player, Uri source);

    void onSubmit(NWiseVideoPlayer player, Uri source);

    void onClickVideoFrame(NWiseVideoPlayer player);
}