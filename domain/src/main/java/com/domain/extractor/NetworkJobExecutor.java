package com.domain.extractor;
import com.domain.executor.UseCaseExecutor;

import rx.Scheduler;
import rx.schedulers.Schedulers;


public class NetworkJobExecutor implements UseCaseExecutor {

    @Override
    public Scheduler getScheduler() {
        return Schedulers.io();
    }
}
