package com.domain.executor;


import rx.Scheduler;

public interface UseCaseExecutor {
    Scheduler getScheduler();
}
