package com.domain.interactor.base;

import com.domain.executor.PostExecutionThread;
import com.domain.executor.UseCaseExecutor;

import rx.Observable;

public abstract class ObservableUseCase<R, Q> extends UseCase<Observable, Q> {

    private final Observable.Transformer<? super R, ? extends R> schedulersTransformer;

    public ObservableUseCase(UseCaseExecutor useCaseExecutor,
                             PostExecutionThread postExecutionThread) {
        super(useCaseExecutor, postExecutionThread);
        schedulersTransformer = rObservable -> rObservable
                .subscribeOn(useCaseExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler());
    }

    @Override
    public Observable<R> execute(Q param) {
        return interact(param).compose(getSchedulersTransformer());
    }

    @Override
    protected abstract Observable<R> interact(Q param);

    public Observable.Transformer<? super R, ? extends R> getSchedulersTransformer() {
        return schedulersTransformer;
    }
}
