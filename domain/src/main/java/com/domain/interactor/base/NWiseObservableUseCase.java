package com.domain.interactor.base;
import com.domain.executor.PostExecutionThread;
import com.domain.executor.UseCaseExecutor;
import com.domain.repsitory.NWiseApi;


public abstract class NWiseObservableUseCase<R, Q> extends ObservableUseCase<R, Q> {
    protected NWiseApi api;

    public NWiseObservableUseCase(UseCaseExecutor useCaseExecutor, PostExecutionThread postExecutionThread,
                                  NWiseApi api) {
        super(useCaseExecutor, postExecutionThread);
        this.api = api;
    }

}
