package com.domain.interactor.base;
import com.domain.executor.PostExecutionThread;
import com.domain.executor.UseCaseExecutor;

import rx.Scheduler;

public abstract class UseCase<P, Q> {

    private final UseCaseExecutor useCaseExecutor;
    private final PostExecutionThread postExecutionThread;

    public UseCase(UseCaseExecutor useCaseExecutor,
                   PostExecutionThread postExecutionThread) {
        this.useCaseExecutor = useCaseExecutor;
        this.postExecutionThread = postExecutionThread;
    }

    /**
     * Executes use case. It should/can call {@link #interact(Object)} to get response value.
     */
    public abstract P execute( Q param);

    /**
     * A hook for interacting with given parameter(request value) and returning a response value for each concrete implementation.
     * <p>
     * It should be called inside {@link #execute(Object)}.
     *</p>
     * @param param The request value.
     * @return Returns the response value.
     */
    protected abstract P interact( Q param);

    public Scheduler getUseCaseExecutor() {
        return useCaseExecutor.getScheduler();
    }

    public Scheduler getPostExecutionThread() {
        return postExecutionThread.getScheduler();
    }
}
