package com.domain.interactor.base;
import com.domain.executor.PostExecutionThread;
import com.domain.executor.UseCaseExecutor;

import rx.Completable;

public abstract class CompletableUseCase<Q> extends UseCase<Completable, Q> {

    private final Completable.CompletableTransformer schedulersTransformer;

    public CompletableUseCase(UseCaseExecutor useCaseExecutor,
                              PostExecutionThread postExecutionThread) {
        super(useCaseExecutor, postExecutionThread);
        schedulersTransformer = completable -> completable.subscribeOn(useCaseExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler());
    }

    @Override
    public Completable execute( Q param) {
        return interact(param).compose(getSchedulersTransformer());
    }

    private Completable.CompletableTransformer getSchedulersTransformer() {
        return schedulersTransformer;
    }
}
