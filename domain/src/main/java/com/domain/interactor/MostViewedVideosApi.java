package com.domain.interactor;

import com.domain.executor.PostExecutionThread;
import com.domain.executor.UseCaseExecutor;
import com.domain.interactor.base.NWiseObservableUseCase;
import com.domain.model.MostViewedVideosResponse;
import com.domain.repsitory.NWiseApi;
import javax.inject.Inject;
import javax.inject.Singleton;
import rx.Observable;

@Singleton
public class MostViewedVideosApi extends NWiseObservableUseCase<MostViewedVideosResponse, Void> {

    @Inject
    public MostViewedVideosApi(UseCaseExecutor useCaseExecutor,
                               PostExecutionThread postExecutionThread,
                               NWiseApi api) {
        super(useCaseExecutor, postExecutionThread,  api);
    }


    @Override
    protected Observable<MostViewedVideosResponse> interact(Void param)
    {
        Observable<MostViewedVideosResponse> mostViewedVideosResponseObservable= api.getMovies();
        return mostViewedVideosResponseObservable;
    }
}
