package com.domain.interactor.presenterInteractorsInterface;

public interface ConnectivityManager {
    boolean hasNet();
}
