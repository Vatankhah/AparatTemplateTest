package com.domain.interactor;

import com.domain.executor.PostExecutionThread;
import com.domain.executor.UseCaseExecutor;
import com.domain.interactor.base.NWiseObservableUseCase;
import com.domain.model.MostViewedVideosResponse;
import com.domain.model.VideoShowModel;
import com.domain.model.VideoShowModelResponse;
import com.domain.repsitory.NWiseApi;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

@Singleton
public class ShowVideoApi extends NWiseObservableUseCase<VideoShowModelResponse, VideoShowModel> {

    @Inject
    public ShowVideoApi(UseCaseExecutor useCaseExecutor,
                        PostExecutionThread postExecutionThread,
                        NWiseApi api) {
        super(useCaseExecutor, postExecutionThread,  api);
    }


    @Override
    protected Observable<VideoShowModelResponse> interact(VideoShowModel param)
    {
        return api.videoShow(param.getVideohash());
    }
}
