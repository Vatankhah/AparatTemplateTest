package com.domain.model;

/**
 * Created by Sepide on 2/6/2017.
 */
public class VideoShowModel {
    private String videohash;

    public VideoShowModel(String videohash) {
        this.videohash = videohash;
    }

    public String getVideohash() {
        return videohash;
    }

    public void setVideohash(String videohash) {
        this.videohash = videohash;
    }
}
