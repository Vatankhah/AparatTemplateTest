package com.domain.model;

import java.util.List;

/**
 * Created by Sepide on 2/7/2017.
 */
public class MostViewedVideosResponse {
    public List<MostViewVideo> getMostviewedvideos() {
        return mostviewedvideos;
    }

    public void setMostviewedvideos(List<MostViewVideo> mostviewedvideos) {
        this.mostviewedvideos = mostviewedvideos;
    }

    private List<MostViewVideo> mostviewedvideos;


    public class MostViewVideo {
        private int id;
        private String title;
        private String small_poster;
        private String frame;
        private String uid;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getFrame() {
            return frame;
        }

        public void setFrame(String frame) {
            this.frame = frame;
        }

        public String getSmall_poster() {
            return small_poster;
        }

        public void setSmall_poster(String small_poster) {
            this.small_poster = small_poster;
        }
    }
}
