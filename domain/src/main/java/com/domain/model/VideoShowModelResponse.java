package com.domain.model;

/**
 * Created by Sepide on 2/6/2017.
 */

public class VideoShowModelResponse {

    public VideoShow getVideoshow() {
        return videoshow;
    }

    public void setVideoshow(VideoShow videoshow) {
        this.videoshow = videoshow;
    }

    private VideoShow videoshow;

    public class VideoShow {
        private String file_link;

        public String getFile_link() {
            return file_link;
        }

        public void setFile_link(String file_link) {
            this.file_link = file_link;
        }
    }

}
