package com.domain.repsitory;

import com.domain.model.MostViewedVideosResponse;
import com.domain.model.VideoShowModelResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Sepide on 2/7/2017.
 */

public interface NWiseApi {

   // @FormUrlEncoded
    @POST("mostviewedvideos")
    Observable<MostViewedVideosResponse> getMovies();


    @FormUrlEncoded
    @POST("videoshow")
    Observable<VideoShowModelResponse> videoShow(@Field("videohash") String videoHash);
}
