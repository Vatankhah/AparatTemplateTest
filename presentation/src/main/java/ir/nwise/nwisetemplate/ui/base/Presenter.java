package ir.nwise.nwisetemplate.ui.base;

import android.view.View;

public interface Presenter<V> {

    /*
    * Called when the view attached to the screen
    *
    * @param view the view that presenter interacts with
    */
    void onViewAttached(V view);

    /*
    * Called when view detached from screen
    * */
    void onViewDetached();

    /*
    * Called when a user leaves the view completely
    */
    void onDestroy();

    /*
    * @return Returns true if the view is currently attached to the presenter , otherwise returns false.
    */
    boolean isViewAttached();
}
