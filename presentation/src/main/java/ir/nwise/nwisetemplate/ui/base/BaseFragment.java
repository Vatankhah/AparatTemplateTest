package ir.nwise.nwisetemplate.ui.base;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Sepide on 2/3/2017.
 */

public abstract class BaseFragment<P extends BasePresenter> extends Fragment {
    Unbinder unbinder;
    protected P presenter;

    protected abstract void initPresenter();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (presenter == null)
            initPresenter();
        return inflater.inflate(getLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        unbinder = ButterKnife.bind(this, getView());
        super.onViewCreated(view, savedInstanceState);
        onCreateCompleted();
    }

    /*
    *Add view related here
    */
    protected abstract void onCreateCompleted();

    @LayoutRes
    public abstract int getLayout();


    @Override
    public void onResume() {
        super.onResume();
        presenter.onViewAttached(this);
    }

    @Override
    public void onPause() {
        presenter.onViewDetached();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroyView();
        presenter.onDestroy();
        super.onDestroy();
        if(unbinder!= null)
            unbinder.unbind();
    }
}
