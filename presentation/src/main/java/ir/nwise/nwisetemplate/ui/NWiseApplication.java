package ir.nwise.nwisetemplate.ui;
import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import com.domain.DomainModule;
import com.network.NetworkConfigModule;
import com.network.NetworkModule;

import timber.log.Timber;

/**
 * Created by Sepide on 2/6/2017.
 */

public class NWiseApplication  extends Application {
    private static AppComponent component;

    private static Context context;
    public static Context getAppContext(){
        return  NWiseApplication.context;
    }

    public void onCreate() {
        super.onCreate();
        initTimber();
        initDagger();
    }


    private void initDagger() {
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .domainModule(new DomainModule())
                .networkModule(new NetworkModule())
                .networkConfigModule(new NetworkConfigModule())
                .build();
        component.injectTo(this);

    }

    private void initTimber() {
        Timber.plant(new Timber.DebugTree());
    }

    @NonNull
    public static AppComponent getComponent(Context context) {
        return ((NWiseApplication) context.getApplicationContext()).getComponent();
    }
    @NonNull
    private AppComponent getComponent() {
        return component;
    }



}
