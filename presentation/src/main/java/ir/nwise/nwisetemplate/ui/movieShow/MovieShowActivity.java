package ir.nwise.nwisetemplate.ui.movieShow;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.domain.model.VideoShowModel;
import com.domain.model.VideoShowModelResponse;

import butterknife.BindView;
import ir.nwise.nwisemediaplayer.NWiseVideoCallback;
import ir.nwise.nwisemediaplayer.NWiseVideoPlayer;
import ir.nwise.nwisetemplate.R;
import ir.nwise.nwisetemplate.ui.AppComponent;
import ir.nwise.nwisetemplate.ui.base.BaseActivity;
import ir.nwise.nwisetemplate.ui.movieShow.di.MovieShowModule;

import static ir.nwise.nwisetemplate.ui.dashboard.MostViewedListAdapter.VIDEO_HASH;

/**
 * Created by Sepide on 2/6/2017.
 */

public class MovieShowActivity extends BaseActivity<MovieShowPresenter> implements MovieShowView , NWiseVideoCallback {
    @BindView(R.id.player)
    NWiseVideoPlayer player;
   // private String videoUrl = "http://as8.cdn.asset.aparat.com/aparat-video/c16dcd85e4bfdd6a2f4fab91686d57ec1146420__57428.apt";

    @Override
    protected void injectDependencies(AppComponent component) {
        component.plus(new MovieShowModule()).injectTo(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_movie_show);

    }

    @Override
    public String getExtra(){
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                return "";
            } else {
                return extras.getString(VIDEO_HASH);
            }
    }

    @Override
    public void onVideoShowResult(VideoShowModelResponse result) {

        if (result != null && result.getVideoshow() != null && result.getVideoshow().getFile_link() != null) {
            assert player != null;
            player.setSource(Uri.parse(result.getVideoshow().getFile_link()));
            player.setCallback(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.pause();
    }

    @Override
    public void onStarted(NWiseVideoPlayer player) {

    }

    @Override
    public void onPaused(NWiseVideoPlayer player) {

    }

    @Override
    public void onPreparing(NWiseVideoPlayer player) {

    }

    @Override
    public void onPrepared(NWiseVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(NWiseVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(NWiseVideoPlayer player) {

    }

    @Override
    public void onRetry(NWiseVideoPlayer player, Uri source) {
        Toast.makeText(this, "Retry", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSubmit(NWiseVideoPlayer player, Uri source) {
        Toast.makeText(this, "Submit", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickVideoFrame(NWiseVideoPlayer player) {
        Toast.makeText(this, "Click video frame.", Toast.LENGTH_SHORT).show();
    }
}
