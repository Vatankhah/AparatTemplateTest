package ir.nwise.nwisetemplate.ui.dashboard;

import com.domain.interactor.MostViewedVideosApi;
import com.domain.model.MostViewedVideosResponse;

import javax.inject.Inject;

import ir.nwise.nwisetemplate.ui.base.BasePresenter;
import timber.log.Timber;

/**
 * Created by Sepide on 2/2/2017.
 */
public class DashboardPresenter extends BasePresenter<DashboardView> {

    private MostViewedVideosApi mostViewedVideosApi;
    private MostViewedVideosResponse localMostViewedVideosResponse;

    @Inject
    public DashboardPresenter(MostViewedVideosApi mostViewedVideosApi) {
        this.mostViewedVideosApi = mostViewedVideosApi;
    }

    @Override
    protected void onViewAttachedForFirstTime(DashboardView view) {
        super.onViewAttachedForFirstTime(view);
        loadData();
    }

    private void loadData() {
        addSubscription(mostViewedVideosApi.execute(null).subscribe(response -> {
            if (response.getMostviewedvideos().size() != 0) {
                localMostViewedVideosResponse = response;
                if (isViewAttached())
                    getView().loadData(response);
            }
        }, error -> {
            Timber.e("mostViewedVideosApi error  : " + error);
           // if (isViewAttached())
              //  getView().getErrorMessageBox("server_error");
        }));
    }

    @Override
    public void onViewAttached(DashboardView view) {
        super.onViewAttached(view);
        if (localMostViewedVideosResponse != null)
            loadData();
    }


}
