package ir.nwise.nwisetemplate.ui;
import com.domain.DomainModule;
import com.network.NetworkConfigModule;
import com.network.NetworkModule;

import javax.inject.Singleton;
import dagger.Component;
import ir.nwise.nwisetemplate.ui.dashboard.di.DashboardComponent;
import ir.nwise.nwisetemplate.ui.dashboard.di.DashboardModule;
import ir.nwise.nwisetemplate.ui.movieShow.di.MovieShowComponent;
import ir.nwise.nwisetemplate.ui.movieShow.di.MovieShowModule;

/**
 * Created by Sepide on 2/6/2017.
 */
@Singleton
@Component(modules ={ AppModule.class, DomainModule.class, NetworkConfigModule.class , NetworkModule.class})
public interface AppComponent {
    void injectTo(NWiseApplication application);
    DashboardComponent plus(DashboardModule module);
    MovieShowComponent plus(MovieShowModule module);
}
