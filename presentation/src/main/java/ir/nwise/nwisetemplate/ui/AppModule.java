package ir.nwise.nwisetemplate.ui;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;

import com.domain.di.ForApplication;
import com.domain.executor.PostExecutionThread;
import com.domain.executor.UseCaseExecutor;
import com.domain.extractor.NetworkJobExecutor;
import com.domain.interactor.presenterInteractorsInterface.ConnectivityManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Sepide on 2/6/2017.
 */

@Module
public class AppModule {
    private final NWiseApplication app;

    public AppModule(@NonNull NWiseApplication application) {
        this.app = application;
    }

    @NonNull
    @Provides
    @Singleton
    public NWiseApplication provideApplication() {
        return app;
    }

    @NonNull
    @Provides
    @Singleton
    @ForApplication
    public Context provideContext() {
        return app.getApplicationContext();
    }

    @NonNull
    @Provides
    @Singleton
    public Resources provideResources(@NonNull @ForApplication Context context) {
        return context.getResources();
    }

    @NonNull
    @Provides
    @Singleton
    public UseCaseExecutor provideUseCaseExecutor() {
        return new NetworkJobExecutor();
    }

    @NonNull
    @Provides
    @Singleton
    public PostExecutionThread postExecutionThread() {
        return AndroidSchedulers::mainThread;
    }

//    @NonNull
//    @Provides
//    @Singleton
//    public ConnectivityManager provideConnectivityManager(ConnectivityManagerImp connectivityManager) {
//        return connectivityManager;
//    }
}
