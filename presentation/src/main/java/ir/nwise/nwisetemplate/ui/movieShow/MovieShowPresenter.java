package ir.nwise.nwisetemplate.ui.movieShow;

import com.domain.interactor.ShowVideoApi;
import com.domain.model.VideoShowModel;

import javax.inject.Inject;

import ir.nwise.nwisetemplate.ui.base.BasePresenter;
import timber.log.Timber;

/**
 * Created by Sepide on 2/6/2017.
 */
public class MovieShowPresenter extends BasePresenter<MovieShowView> {

    private ShowVideoApi showVideoApi;
    private String hashVideoId;

    @Override
    protected void onViewAttachedForFirstTime(MovieShowView view) {
        super.onViewAttachedForFirstTime(view);
        if (isViewAttached()) {
            hashVideoId = getView().getExtra();
            showVideo(new VideoShowModel(hashVideoId));
        }
    }

    @Inject
    public MovieShowPresenter(ShowVideoApi showVideoApi) {
        this.showVideoApi = showVideoApi;
    }

    protected void showVideo(VideoShowModel model) {
        if (isViewAttached()) {
            //getView().disableViews();
            //getView().showLoading();
        }
        addSubscription(showVideoApi.execute(model).subscribe(response -> {
            Timber.i("ShowVideoApi  ");
            if (response != null) {
                if (isViewAttached())
                    getView().onVideoShowResult(response);
            }
        }));
    }
}
