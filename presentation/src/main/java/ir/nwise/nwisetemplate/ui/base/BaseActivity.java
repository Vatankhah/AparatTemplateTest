package ir.nwise.nwisetemplate.ui.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import ir.nwise.nwisetemplate.ui.AppComponent;
import ir.nwise.nwisetemplate.ui.NWiseApplication;

public abstract class BaseActivity<P extends BasePresenter> extends AppCompatActivity {
    @Inject
    protected P presenter;
    Unbinder unbinder;

    //protected abstract void initPresenter();
    protected abstract void injectDependencies(AppComponent component);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        injectDependencies(NWiseApplication.getComponent(this));
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onViewAttached(this);
    }

    @Override
    protected void onPause() {
        presenter.onViewDetached();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {

        super.setContentView(layoutResID);
        unbinder = ButterKnife.bind(this);
    }

}
