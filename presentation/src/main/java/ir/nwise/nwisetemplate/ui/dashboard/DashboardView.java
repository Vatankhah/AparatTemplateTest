package ir.nwise.nwisetemplate.ui.dashboard;

import com.domain.model.MostViewedVideosResponse;

/**
 * Created by Sepide on 2/2/2017.
 */
public interface DashboardView {
    void loadData(MostViewedVideosResponse response);
}
