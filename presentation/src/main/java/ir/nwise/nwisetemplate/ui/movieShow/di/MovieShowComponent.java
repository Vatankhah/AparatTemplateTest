package ir.nwise.nwisetemplate.ui.movieShow.di;

import dagger.Subcomponent;
import ir.nwise.nwisetemplate.ui.base.PerActivity;
import ir.nwise.nwisetemplate.ui.dashboard.di.DashboardModule;
import ir.nwise.nwisetemplate.ui.movieShow.MovieShowActivity;

/**
 * Created by Sepide on 2/7/2017.
 */

@PerActivity
@Subcomponent(modules = MovieShowModule.class)
public interface MovieShowComponent {
    void injectTo(MovieShowActivity activity );
}
