package ir.nwise.nwisetemplate.ui.movieShow;

import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Created by Sepide on 2/6/2017.
 */

class CustomMediaPlayer extends MediaPlayer
{
    String dataSource;

    @Override
    public void setDataSource(String path) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException
    {
        // TODO Auto-generated method stub
        super.setDataSource(path);
        dataSource = path;
    }

    public String getDataSource()
    {
        return dataSource;
    }
}