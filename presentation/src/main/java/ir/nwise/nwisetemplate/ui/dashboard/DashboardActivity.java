package ir.nwise.nwisetemplate.ui.dashboard;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.domain.model.MostViewedVideosResponse;

import butterknife.BindView;
import ir.nwise.nwisetemplate.R;
import ir.nwise.nwisetemplate.ui.AppComponent;
import ir.nwise.nwisetemplate.ui.base.BaseActivity;
import ir.nwise.nwisetemplate.ui.dashboard.di.DashboardModule;

import static android.R.id.list;

/**
 * Created by Sepide on 2/2/2017.
 */
public class DashboardActivity extends BaseActivity<DashboardPresenter> implements DashboardView , RecyclerView.OnItemTouchListener {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void injectDependencies(AppComponent component) {
        component.plus(new DashboardModule()).injectTo(this);
    }

    private StickyHeaderDecoration decor ;
    @Override
    public void loadData(MostViewedVideosResponse response) {
        MostViewedListAdapter adapter = new MostViewedListAdapter(getApplicationContext(), response.getMostviewedvideos());
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//        CustomLinearLayoutManager layoutManager =new CustomLinearLayoutManager(getApplicationContext(),1,false);
//        recyclerView.setLayoutManager(mLayoutManager);



        final DividerDecoration divider = new DividerDecoration.Builder(this)
                .setHeight(R.dimen.default_divider_height)
                .setPadding(R.dimen.default_divider_padding)
                .setColorResource(R.color.default_header_color)
                .build();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(divider);

       // setAdapterAndDecor(mList);

        //final StickyTestAdapter adapter = new StickyTestAdapter(this.getActivity());
        decor = new StickyHeaderDecoration(adapter);

        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(decor, 1);
        recyclerView.addOnItemTouchListener(this);


        recyclerView.setItemAnimator(new DefaultItemAnimator());
       // recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        // really bad click detection just for demonstration purposes
        // it will not allow the list to scroll if the swipe motion starts
        // on top of a header
        View v = rv.findChildViewUnder(e.getX(), e.getY());
        return v == null;
//        return rv.findChildViewUnder(e.getX(), e.getY()) != null;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        // only use the "UP" motion event, discard all others
        if (e.getAction() != MotionEvent.ACTION_UP) {
            return;
        }

        // find the header that was clicked
        View view = decor.findHeaderViewUnder(e.getX(), e.getY());

        if (view instanceof TextView) {
            Toast.makeText(this, ((TextView) view).getText() + " clicked", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        // do nothing
    }
}
