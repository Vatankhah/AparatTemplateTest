package ir.nwise.nwisetemplate.ui.base;


import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public class BasePresenter<V> extends AbstractPresenter<V> {
    protected boolean isFirstTime() {
        return isFirstTime;
    }

    private boolean isFirstTime = true;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    @Override
    public void onViewAttached(V view) {
        super.onViewAttached(view);
        if(isFirstTime) {
            isFirstTime = false;
            onViewAttachedForFirstTime(view);
        }
    }

    protected void addSubscription(Subscription subscription) {
        compositeSubscription.add(subscription);
    }


    @Override
    public void onViewDetached() {
        super.onViewDetached();
    }

    protected void onViewAttachedForFirstTime(V view)
    {

    }


}
