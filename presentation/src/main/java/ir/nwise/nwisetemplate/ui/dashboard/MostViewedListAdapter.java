
package ir.nwise.nwisetemplate.ui.dashboard;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.domain.model.MostViewedVideosResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.nwise.nwisetemplate.R;
import ir.nwise.nwisetemplate.ui.movieShow.MovieShowActivity;


public class MostViewedListAdapter extends RecyclerView.Adapter<MostViewedListAdapter.ViewHolder>
        implements StickyHeaderAdapter<MostViewedListAdapter.HeaderHolder> {

    public static final String VIDEO_HASH = "VIDEO_HASH";
    Context context;
    private List<MostViewedVideosResponse.MostViewVideo> mostViewedVideos;

    public MostViewedListAdapter(Context context, List<MostViewedVideosResponse.MostViewVideo> noteModels) {
        this.context = context;
        this.mostViewedVideos = noteModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_aparat, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MostViewedVideosResponse.MostViewVideo model = mostViewedVideos.get(position);
        holder.lblTitle.setText(model.getTitle());
        Picasso.with(context).load(model.getSmall_poster()).into(holder.imgSmallPoster);

        holder.mView.setOnClickListener(view -> {
            Intent intent = new Intent(context, MovieShowActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(VIDEO_HASH, model.getUid());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mostViewedVideos.size();
    }

    @Override
    public long getHeaderId(int position) {
        if (position == 0) { // don't show header for first item
            return StickyHeaderDecoration.NO_HEADER_ID;
        }
        if (position == 3)
            return (long) position;
        return -1;
    }

    private LayoutInflater mInflater;

    @Override
    public HeaderHolder onCreateHeaderViewHolder(ViewGroup parent) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.header_test, parent, false);
        // final View view = mInflater.inflate(R.layout.header_test, parent, false);
        return new HeaderHolder(itemView);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderHolder viewholder, int position) {
        final MostViewedVideosResponse.MostViewVideo model = mostViewedVideos.get(position);
        viewholder.lblTitle.setText(model.getTitle());
        Picasso.with(context).load(model.getSmall_poster()).into(viewholder.imgSmallPoster);

        viewholder.mView.setOnClickListener(view -> {
            Intent intent = new Intent(context, MovieShowActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(VIDEO_HASH, model.getUid());
            context.startActivity(intent);
        });
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        @Nullable
        @BindView(R.id.imgSmallPoster)
        ImageView imgSmallPoster;
        @Nullable
        @BindView(R.id.lblTitle)
        TextView lblTitle;


        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView = itemView;
        }
    }


    class HeaderHolder extends RecyclerView.ViewHolder {
        public final View mView;
        @Nullable
        @BindView(R.id.imgHeaderSmallPoster)
        ImageView imgSmallPoster;
        @Nullable
        @BindView(R.id.lblHeaderTitle)
        TextView lblTitle;


        public HeaderHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mView = itemView;
        }
    }
}
