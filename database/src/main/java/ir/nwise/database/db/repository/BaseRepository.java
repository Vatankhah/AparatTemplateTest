package ir.nwise.database.db.repository;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ir.nwise.database.db.BaseTable;
import ir.nwise.database.db.NoteStarDatabase;
import ir.nwise.database.models.BaseEntity;


public class BaseRepository<T extends BaseEntity> {

    protected final NoteStarDatabase dbInstance;
    protected Context mContext;
    protected BaseTable<T> table;

    public BaseRepository(Context mContext) {
        this.mContext = mContext;
        dbInstance = new NoteStarDatabase(mContext);
        table = null;
    }

    public int insert(T entity)
    {
        SQLiteDatabase db = dbInstance.getWritableDatabase();
        ContentValues cv = table.entityToContentValues(entity);
        int id = (int)db.insert(table.getName(),null,cv);
        db.close();
        return id;
    }

    public int update(T entity)
    {
        SQLiteDatabase db = dbInstance.getWritableDatabase();
        String whereClause = "_id=" + entity.getId() ;
        ContentValues cv = table.entityToContentValues(entity);
        int result = db.update(table.getName(), cv , whereClause ,null);
        db.close();
        return result;
    }


    public int Delete(int id)
    {
        SQLiteDatabase db = dbInstance.getWritableDatabase();
        String whereClause = "_id=" + id ;
        int result = db.delete(table.getName(), whereClause, null);
        db.close();
        return result;
    }

    public void CustomDelete(String sql , Object[] selectionArgs)
    {
        SQLiteDatabase db = dbInstance.getWritableDatabase();
        db.execSQL(sql,selectionArgs);;
        db.close();
    }


    public T getById(int id)
    {
        String whereClause = "_id =" + id;
        List<T> entities = getAll(whereClause);
        if(!entities.isEmpty())
        {
            return entities.get(0);
        }
        return null;
    }

    public List<T> getAll(String whereClause , String orderBy , String groupBy , String limit)
    {
        ArrayList<T> results = new ArrayList<T>();
        SQLiteDatabase db = dbInstance.getWritableDatabase();
        Cursor cursor = db.query(table.getName(), table.getAllColumns(), whereClause, null, groupBy, null, orderBy, limit);
        try
        {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                T entity = table.cursorToEntity(cursor);
                results.add(entity);
                cursor.moveToNext();
            }
        }
        finally
        {
            if (cursor != null && !cursor.isClosed())
                cursor.close();
            if (db != null )
                db.close();
        }
        return results;
    }

    public List<T> getSelectedColumnAll(String sql , String[] selectionArgs )
    {
        ArrayList<T> results = new ArrayList<T>();
        SQLiteDatabase db = dbInstance.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, selectionArgs);
        try
        {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                T entity = table.cursorToEntity(cursor);
                results.add(entity);
                cursor.moveToNext();
            }
        }
        finally
        {
            if (cursor != null && !cursor.isClosed())
                cursor.close();
            if (db != null )
                db.close();
        }
        return results;
    }

    public List<T> getAll(String whereClause , String orderBy)
    {
        return getAll(whereClause,orderBy,null,null);
    }

    public List<T>  getAll(String whereClause)
    {
        return getAll(whereClause,null);
    }

    public List<T> getAll()
    {
        return getAll(null);
    }

    public NoteStarDatabase getDatabase(){return dbInstance;}

    public List<T> getCustomQuery(String query , String[] selectionArgs)
    {
        List<T> results = new ArrayList<T>();
        SQLiteDatabase db = dbInstance.getWritableDatabase();
        Cursor cursor = db.rawQuery(query , selectionArgs );
        try
        {
            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                T entity = table.cursorToEntity(cursor);
                results.add(entity);
                cursor.moveToNext();
            }
        }
        finally
        {
            if (cursor != null && !cursor.isClosed())
                cursor.close();
            if (db != null )
                db.close();
        }
        return results;
    }

    protected String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            //throw new RuntimeException("No placeholders");
            return "";
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }
}
