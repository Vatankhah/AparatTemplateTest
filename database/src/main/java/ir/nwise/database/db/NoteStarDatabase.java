package ir.nwise.database.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class NoteStarDatabase extends SQLiteOpenHelper {
    // Database name
    private static final String DATABASE_NAME = Constants.DATABASE_NAME;
    // Database version aligned if possible to software version
    private static final int DATABASE_VERSION = 1;

    private final BaseTable[] tables = new BaseTable[]
            {
                    new NoteTable(),
            };


    public NoteStarDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Log.i(Constants.TAG, "Database creation");
            for (int i = 0; i < tables.length; i++) {
                tables[i].onCreate(db);
            }
        } catch (Exception e) {
            Log.e(Constants.TAG, "Database creation failed", e);
        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(Constants.TAG, "Upgrading database version from " + oldVersion + " to " + newVersion);

        //  UpgradeProcessor.process(oldVersion, newVersion);
        try {
            for (int i = 0; i < tables.length; i++) {
                tables[i].onUpgrade(db, oldVersion, newVersion);
            }

            Log.i(Constants.TAG, "Database upgrade successful");

        } catch (Exception e) {
            Log.e(Constants.TAG, "Database upgrade failed", e);
        }
    }
}
