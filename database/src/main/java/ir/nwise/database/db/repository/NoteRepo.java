package ir.nwise.database.db.repository;

import android.content.Context;

import java.util.List;

import ir.nwise.database.db.NoteTable;
import ir.nwise.database.models.NoteModel;

import static ir.nwise.database.db.NoteTable.COLUMN_NOTE_ID;
import static ir.nwise.database.db.NoteTable.COLUMN_NOTE_PARENT_ID;
import static ir.nwise.database.db.NoteTable.TABLE_NOTE;


public class NoteRepo extends BaseRepository<NoteModel> {
    public NoteRepo(Context mContext) {
        super(mContext);
        table = new NoteTable();
    }

    public int getChildren(int id) {
        String sql = "SELECT " + COLUMN_NOTE_ID + " FROM " + TABLE_NOTE + " WHERE " + COLUMN_NOTE_PARENT_ID + " = " + id;
        List<NoteModel> results = getCustomQuery(sql, new String[]{"_id"});
        return results.size();
    }

    public void deleteSelected(Integer[] ids )
    {
        String sql = "delete from " + TABLE_NOTE+ " WHERE "+ COLUMN_NOTE_ID +"  in ("+ makePlaceholders(ids.length)+")";
        CustomDelete(sql,ids);

    }
}
