package ir.nwise.database.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import ir.nwise.database.models.NoteModel;


public class NoteTable extends BaseTable<NoteModel> {
    public static final String TABLE_NOTE = "notes";
    public static final String COLUMN_NOTE_ID = "_id";
    public static final String COLUMN_NOTE_TITLE = "title";
    public static final String COLUMN_NOTE_CONTENT = "content";
    public static final String COLUMN_NOTE_TYPE = "type";
    public static final String COLUMN_NOTE_PARENT_ID = "parent_id";


    private static final String DATABASE_CREATE = "create table "
            + TABLE_NOTE + "("
            + COLUMN_NOTE_ID + " integer primary key autoincrement, "
            + COLUMN_NOTE_TITLE + " text not null,"
            + COLUMN_NOTE_CONTENT + " text null,"
            + COLUMN_NOTE_TYPE + " text not null,"
            + COLUMN_NOTE_PARENT_ID + " integer null"
            + ");";

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            Log.i(Constants.TAG, "Creating " + TABLE_NOTE + " table...");
            db.execSQL(DATABASE_CREATE);
        } catch (Exception e) {
            Log.e(Constants.TAG, "Error executing command: " + DATABASE_CREATE, e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("DATABASE", "Updating " + TABLE_NOTE + " table ..");
        try {
            db.execSQL("drop table if exists " + TABLE_NOTE);
            onCreate(db);
            Log.i(Constants.TAG, "Database upgrade successful");
        } catch (Exception e) {
            Log.e(Constants.TAG, "Error executing command: " + DATABASE_CREATE, e);
        }
    }

    @Override
    public NoteModel cursorToEntity(Cursor cursor) {
        NoteModel model = new NoteModel();
        model.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_NOTE_ID)));
        model.setParentId(cursor.getInt(cursor.getColumnIndex(COLUMN_NOTE_PARENT_ID)));
        model.setType(cursor.getString(cursor.getColumnIndex(COLUMN_NOTE_TYPE)));
        model.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_NOTE_TITLE)));
        model.setContent(cursor.getString(cursor.getColumnIndex(COLUMN_NOTE_CONTENT)));
        return model;
    }

    @Override
    public ContentValues entityToContentValues(NoteModel entity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NOTE_TITLE, entity.getTitle());
        contentValues.put(COLUMN_NOTE_CONTENT, entity.getContent());
        contentValues.put(COLUMN_NOTE_TYPE, entity.getType());
        contentValues.put(COLUMN_NOTE_PARENT_ID, entity.getParentId());
        return contentValues;
    }

    @Override
    public String getName() {
        return TABLE_NOTE;
    }

    @Override
    public String[] getAllColumns() {
        return new String[]{
                COLUMN_NOTE_ID,
                COLUMN_NOTE_TITLE,
                COLUMN_NOTE_CONTENT,
                COLUMN_NOTE_TYPE,
                COLUMN_NOTE_PARENT_ID
        };
    }
}
