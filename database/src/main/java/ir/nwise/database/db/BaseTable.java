package ir.nwise.database.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public abstract class BaseTable<T> {
    public abstract void onCreate(SQLiteDatabase database);
    public abstract void onUpgrade(SQLiteDatabase database , int oldVersion , int newVersion);
    public abstract T cursorToEntity(Cursor cursor);
    public abstract ContentValues entityToContentValues(T entity);
    public abstract String getName();
    public abstract String[] getAllColumns();
}
