package ir.nwise.database.db;


public interface Constants {


    String DATABASE_NAME = "startnote";
    String TAG = "StartNotes";
    String PARENT_ID = "parent_id";
    String NOTE_ID = "note_id";
    String PAINT_ID = "paint_id";
}
