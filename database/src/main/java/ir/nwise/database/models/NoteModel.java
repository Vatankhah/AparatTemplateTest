package ir.nwise.database.models;

/**
 * Created by Sepideh on 1/15/2017.
 */

public class NoteModel extends BaseEntity {

    private String title;
    private String content;
    private int parentId;
    private String type;

    public NoteModel() {
    }

    public NoteModel(String title, String content, int parentId, String type) {
        this.title = title;
        this.content = content;
        this.parentId = parentId;
        this.type = type;
    }


    public NoteModel(String title, String content, int parentId, String type , int _id) {
        this.title = title;
        this.content = content;
        this.parentId = parentId;
        this.type = type;
        this.setId(_id);
    }

    public String getType() {return type;}

    public void setType(String type) {this.type = type;}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
