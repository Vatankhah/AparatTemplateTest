package com.network;

import com.domain.repsitory.NWiseApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.concurrent.TimeUnit;
import javax.inject.Named;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkConfigModule {

    private static final String APARAT_RETROFIT = "AparatRetrofit";
    private static final String APARAT_URL = "http://www.aparat.com/etc/api/";

    @Provides
    @Singleton
    @Named(APARAT_RETROFIT)
    public Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(APARAT_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Singleton
    @Provides
    public NWiseApi provideNWiseApi(@Named(APARAT_RETROFIT) Retrofit retrofit) {
        return retrofit.create(NWiseApi.class);
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new GsonBuilder().serializeNulls().setVersion(1).create();
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttp() {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS).addInterceptor(chain -> {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder();
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                });
        return builder.build();
    }
}
